package com.example.topnewsmvvmkotlin.utils

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics

/**
 * Tracks an event in Firebase configured with parameters provided
 *
 * @param eventName The event name
 * @param screenName The [AnalyticsEventBundleData.SCREEN_NAME_KEY] value
 * @param action The [AnalyticsEventBundleData.ACTION_KEY] value
 */
fun Fragment.firebaseTrackEvent(eventName: String, screenName: String, action: String, extra: Map<String, String> = emptyMap()) {
    activity?.firebaseTrackEvent(eventName, screenName, action, extra)
}


fun Activity.firebaseTrackEvent(eventName: String, screenName: String, action: String, extra: Map<String, String>) {
    val bundle = AnalyticsEventBundleData().createBundle(
        screenName = screenName,
        action = action)
    extra.forEach { mapEntry ->
        bundle.putString(mapEntry.key, mapEntry.value)
    }
    firebaseLogEvent(
        eventName = eventName,
        bundle = bundle)
}

private fun Activity.firebaseLogEvent(eventName: String, bundle: Bundle) {
    FirebaseAnalytics.getInstance(this).logEvent(eventName, bundle)
}

class AnalyticsEventBundleData {

    companion object {
        const val SCREEN_NAME_KEY = "screen_name"
        const val ACTION_KEY = "action"
        const val ELEMENT_KEY = "element"
        const val VALUE_KEY = "value"
        const val DATA_KEY = "data"
    }

    fun createBundle(screenName: String = "",
                     action: String = "",
                     element: String = "",
                     value: String = "",
                     data: String = ""): Bundle {
        val bundle = Bundle()
        if (action.isNotEmpty()) bundle.putString(ACTION_KEY, action)
        if (element.isNotEmpty()) bundle.putString(ELEMENT_KEY, element)
        if (screenName.isNotEmpty()) bundle.putString(SCREEN_NAME_KEY, screenName)
        if (value.isNotEmpty()) bundle.putString(VALUE_KEY, value)
        if (data.isNotEmpty()) bundle.putString(DATA_KEY, value)
        return bundle
    }
}