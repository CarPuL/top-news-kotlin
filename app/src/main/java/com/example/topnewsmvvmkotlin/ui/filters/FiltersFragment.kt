package com.example.topnewsmvvmkotlin.ui.filters

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.topnewsmvvmkotlin.R
import com.example.topnewsmvvmkotlin.ui.MainActivity
import com.example.topnewsmvvmkotlin.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_filters.*
import kotlinx.android.synthetic.main.navigationdrawer_body.*
import kotlinx.android.synthetic.main.navigationdrawer_body.editKeyWord
import kotlinx.android.synthetic.main.navigationdrawer_body.spinnerFilterCategory
import kotlinx.android.synthetic.main.navigationdrawer_body.spinnerFilterCountry
import kotlinx.android.synthetic.main.navigationdrawer_body.spinnerFilterLanguage
import kotlinx.android.synthetic.main.navigationdrawer_body.spinnerFilterSource

class FiltersFragment : Fragment(), AdapterView.OnItemSelectedListener, View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.builtSpinners()
        spinnerFilterSource.onItemSelectedListener = this
        fabFilters.setOnClickListener(this)

        (activity as MainActivity).toolBar.title = "Filters"
        Log.i("carpul", "OnViewCreated")
    }

    override fun onResume() {
        super.onResume()
        firebaseTrackEvent(
            "filte_success_access",
            "filters",
            "access")
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        Log.i("carpul", "On Item Selected")
        if (position > 0) {
            spinnerFilterCountry.hide()
            spinnerFilterCategory.hide()
        }else{
            spinnerFilterCountry.show()
            spinnerFilterCategory.show() }
    }

    override fun onClick(v: View) {
        Log.i("carpul", "On Click in filters")
        val valuesFiltersToHomeFragment =
                "${spinnerFilterCountry.getValue(R.array.countryValues)}," +
                "${spinnerFilterCategory.getValue(R.array.categoryValues)}," +
                "${spinnerFilterSource.getValue(R.array.sourceValues)}," +
                "${spinnerFilterLanguage.getValue(R.array.languageValues)}," +
                "${editKeyWord.text} "

        if(valuesFiltersToHomeFragment != Constants.NO_FILTERS_SELECTED) {

            firebaseTrackEvent(
                "filter_success_click",
                "filter_news",
                "click",
            extra = mapOf("filter" to valuesFiltersToHomeFragment))

            val passValuesFilters
                    = FiltersFragmentDirections
                     .actionFiltersFragmentToHomeFragment()
                     .setDefaulValuesFilter(valuesFiltersToHomeFragment)

            Navigation.findNavController(v).navigate(passValuesFilters)
        }
        else {

            val yOffSet = (activity as MainActivity).navBottomNavigation.getYOffsetToast()
            Log.i("Carpul", "onClick: $yOffSet")
            makeToast(context, resources.getString(R.string.wrongChoice), yOffSet)
        }

    }
}
