package com.example.topnewsmvvmkotlin.ui.filters

//Model to represent item in Spinner
data class ModelSpinner (val textSpinner: String, val imageSpinner: Int)